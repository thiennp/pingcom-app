import { Injectable } from '@angular/core'
import { Http, Headers } from '@angular/http'
import { AppService } from './app.service'
import * as _ from 'lodash'
import * as utf8 from 'utf8'

@Injectable()

export class HttpClient {

    constructor(
        private http: Http,
        private appService: AppService
    ) { }

    /**
     * Get current user via localStorage
     */
    getAuth() {
        let auth: any

        // Check if user is logged in
        if (localStorage.getItem('pingcom-app-E') && localStorage.getItem('pingcom-app-A') && localStorage.getItem('pingcom-app-C')) {
            try {
                auth = {
                    C: localStorage.getItem('pingcom-app-C'),
                    A: localStorage.getItem('pingcom-app-A'),
                    E: JSON.parse(window.atob(localStorage.getItem('pingcom-app-E')))
                }
            } catch (err) {
                // If user is not in JSON formatted string, then ignore
            }
        }

        return auth
    }

    createAuthorizationHeader(headers: Headers) {
        var auth = this.getAuth()
        if (auth) {
            try {
                headers.append('Authorization', 'Bearer ' + auth.E.token)
                headers.append('Wap', 'true')
            } catch (err) {
                this.appService.log(err, 1)
            }
        }
    }

    get(url) {
        return new Promise((resolve, reject) => {
            let headers = new Headers()
            this.createAuthorizationHeader(headers)
            return this.http.get('https://apiwapxlt.pingcom.vn/' + url, {
                headers: headers
            }).subscribe((result: any) => {
                const dataArrayStr = result['_body'].split('::')

                _.each(dataArrayStr, (item) => {
                    const dataItemValue = item.split('=')
                    result[dataItemValue[0]] = dataItemValue[1]
                })
                if (result.E) {
                    try {
                        result.E = JSON.parse(utf8.decode(window.atob(result.E)))
                    } catch (err) {
                        this.appService.log(err, 1)
                    }
                }
                if (result.D) {
                    try {
                        result.D = utf8.decode(window.atob(result.D))
                        this.appService.log(result.D, 1)
                    } catch (err) {
                        this.appService.log(err, 1)
                    }
                }
                this.appService.log(result, 3)
                resolve(result)
            }, err => {
                this.appService.log(err, 1)
                reject(err)
            })
        })
    }

    post(url, data) {
        return new Promise((resolve, reject) => {
            let headers = new Headers()
            this.createAuthorizationHeader(headers)
            return this.http.post('https://apiwapxlt.pingcom.vn/' + url, data, {
                headers: headers
            }).subscribe((result: any) => {
                if (result.E) {
                    try {
                        result.E = JSON.parse(window.atob(result.E))
                    } catch (err) {
                        this.appService.log(err, 1)
                    }
                }
                if (result.D) {
                    try {
                        result.D = window.atob(result.D)
                    } catch (err) {
                        this.appService.log(err, 1)
                    }
                }
                this.appService.log(result, 3)
                resolve(result)
            }, err => {
                this.appService.log(err, 1)
                reject(err)
            })
        })
    }
}

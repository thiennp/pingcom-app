import { Injectable } from '@angular/core'
import { HttpClient } from '../app.http'
import * as _ from 'lodash'

@Injectable()

export class HeaderService {
    constructor(
        private http: HttpClient
    ) { }

    /**
     * Get general data
     */
    getGeneralData() {
        const auth = this.http.getAuth()
        return this.http.get('00?&dv=11a&khid=' + auth.A + '&nn=vi')
    }
}
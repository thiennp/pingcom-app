import { Component } from '@angular/core'
import { AppService } from '../app.service'
import { UserService } from '../common/user.service'
import { HeaderService } from './header.service'
import { HttpClient } from '../app.http'

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
    providers: [AppService, UserService, HeaderService, HttpClient]
})

export class AppHeaderComponent {
    user: any
    notificationCounts: any

    constructor(
        private appService: AppService,
        private userService: UserService,
        private headerService: HeaderService,
        private http: HttpClient
    ) {
        const auth = this.http.getAuth()
        if (auth) {
            try {
                this.user = auth.E
            } catch (err) {
                this.appService.log(err, 1)
            }
        }
        if (this.user) {
            this.headerService.getGeneralData().then((data: any) => {
                try {
                    this.notificationCounts = data.E.tong
                } catch (err) {
                    this.appService.log(err, 1)
                }
            }, err => {
                this.appService.log(err, 1)
            })
        }
    }

    showLogin() {
        const modalRef = this.userService.onShowLogin()
    }
}
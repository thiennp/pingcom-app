import { BrowserModule } from '@angular/platform-browser'
import { RouterModule } from '@angular/router'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { QrScannerModule, QrScannerComponent } from 'angular2-qrscanner'
import { NgxQRCodeModule } from 'ngx-qrcode2'
import { MomentModule } from 'angular2-moment'

import { AppComponent } from './app.component'
import { AppRoutes } from './app.routes'
import { AppLoginComponent } from './login/login.component'
import { AppCommentComponent } from './content/comment/comment.component'
import { AppAdsComponent } from './ads/ads.component'
import { AppHeaderComponent } from './header/header.component'
import { AppFooterComponent } from './footer/footer.component'
import { AppContentComponent } from './content/content.component'
import { AppProfileComponent } from './content/profile/profile.component'
import { AppProfileListComponent } from './content/profile/list/profile.list.component'
import { AppProfileFavouritesPromotionComponent } from './content/profile/favourites/promotion/profile.favourites.promotion.component'
import { AppProfileSettingComponent } from './content/profile/setting/profile.setting.component'
import { AppProfileFavouritesComponent } from './content/profile/favourites/profile.favourites.component'
import { AppProfileFavouritesProductComponent } from './content/profile/favourites/product/profile.favourites.product.component'
import { AppProfileHistoryComponent } from './content/profile/history/profile.history.component'
import { AppProfileHistoryPointComponent } from './content/profile/history/point/profile.history.point.component'
import { AppProfileHistoryProductComponent } from './content/profile/history/product/profile.history.product.component'
import { AppProfileHistoryPromotionComponent } from './content/profile/history/promotion/profile.history.promotion.component'
import { AppProfileHistoryGiftComponent } from './content/profile/history/gift/profile.history.gift.component'
import { AppProfileHistoryVoucherComponent } from './content/profile/history/voucher/profile.history.voucher.component'
import { AppBenefitsComponent } from './content/benefits/benefits.component'
import { AppBenefitsListComponent } from './content/benefits/list/benefits.list.component'
import { AppBenefitsListClosestComponent } from './content/benefits/list/closest/benefits.list.closest.component'
import { AppBenefitsListHottestComponent } from './content/benefits/list/hottest/benefits.list.hottest.component'
import { AppBenefitsListLatestComponent } from './content/benefits/list/latest/benefits.list.latest.component'
import { AppBenefitsDetailComponent } from './content/benefits/detail/benefits.detail.component'
import { AppCardsComponent } from './content/cards/cards.component'
import { AppCardsListComponent } from './content/cards/list/cards.list.component'
import { AppCardsDetailComponent } from './content/cards/detail/cards.detail.component'
import { AppCardsNewComponent } from './content/cards/new/cards.new.component'
import { AppCardsProductComponent } from './content/cards/product/cards.product.component'
import { AppCardsPromotionComponent } from './content/cards/promotion/cards.promotion.component'
import { AppCodeComponent } from './content/code/code.component'
import { AppGiftsComponent } from './content/gifts/gifts.component'
import { AppNotificationsComponent } from './content/notifications/notifications.component'
import { AppNotificationsPersonalComponent } from './content/notifications/personal/notifications.personal.component'
import { AppNotificationsPublicComponent } from './content/notifications/public/notifications.public.component'
import { AppVouchersComponent } from './content/vouchers/vouchers.component'
import { AppVouchersListComponent } from './content/vouchers/list/vouchers.list.component'
import { AppVouchersListMineComponent } from './content/vouchers/list/mine/vouchers.list.mine.component'
import { AppVouchersListAllComponent } from './content/vouchers/list/all/vouchers.list.all.component'
import { AppVouchersDetailComponent } from './content/vouchers/detail/vouchers.detail.component'
import { AppIconsComponent } from './supports/icons/icons.component'

@NgModule({
    declarations: [
        QrScannerComponent,
        AppComponent,
        AppCommentComponent,
        AppAdsComponent,
        AppHeaderComponent,
        AppFooterComponent,
        AppContentComponent,
        AppProfileComponent,
        AppProfileListComponent,
        AppProfileFavouritesProductComponent,
        AppProfileFavouritesPromotionComponent,
        AppProfileHistoryComponent,
        AppProfileHistoryPointComponent,
        AppProfileHistoryProductComponent,
        AppProfileHistoryPromotionComponent,
        AppProfileHistoryGiftComponent,
        AppProfileHistoryVoucherComponent,
        AppProfileSettingComponent,
        AppProfileFavouritesComponent,
        AppBenefitsComponent,
        AppBenefitsListComponent,
        AppBenefitsListClosestComponent,
        AppBenefitsListHottestComponent,
        AppBenefitsListLatestComponent,
        AppBenefitsDetailComponent,
        AppCardsComponent,
        AppCardsListComponent,
        AppCardsDetailComponent,
        AppCardsNewComponent,
        AppCardsProductComponent,
        AppCardsPromotionComponent,
        AppCodeComponent,
        AppGiftsComponent,
        AppNotificationsComponent,
        AppNotificationsPersonalComponent,
        AppNotificationsPublicComponent,
        AppVouchersComponent,
        AppVouchersListComponent,
        AppVouchersListMineComponent,
        AppVouchersListAllComponent,
        AppVouchersDetailComponent,
        AppLoginComponent,
        AppIconsComponent
    ],
    imports: [
        BrowserModule,
        NgbModule.forRoot(),
        RouterModule.forRoot(AppRoutes),
        FormsModule,
        HttpModule,
        MomentModule,
        NgxQRCodeModule
    ],
    providers: [],
    bootstrap: [AppComponent],
    entryComponents: [AppLoginComponent]
})
export class AppModule {
}
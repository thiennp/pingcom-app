import { Component, Input } from '@angular/core'
import { AppService } from '../../app.service'
import { HttpClient } from '../../app.http'
import { CommentService } from './comment.service'
import * as _ from 'lodash'

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.css'],
    providers: [AppService, CommentService]
})

export class AppCommentComponent {
    @Input() id: any
    @Input() type: any
    @Input() name: any
    end: Boolean
    firstLoading: Boolean
    list: any[]
    loading: Boolean
    page: any
    result: any
    scrollItv: any
    user: any

    constructor(
        private appService: AppService,
        private commentService: CommentService,
        private http: HttpClient
    ) {
        this.page = 1

        const auth = this.http.getAuth()
        if (auth) {
            try {
                this.user = auth.E
            } catch (err) {
                this.appService.log(err, 1)
            }
        }
    }

    ngOnInit() {
        this.getData()
        this.scrollItv = setInterval(() => {
            if (window.scrollY > document.body.scrollHeight - window.innerHeight - 300 && this.page > 1) {
                this.getData()
            }
        }, 100)
    }

    ngOnDestroy() {
        clearInterval(this.scrollItv)
    }

    getData() {
        if (!this.loading && !this.end) {
            this.loading = true
            if (this.page == 1) {
                this.list = []
                this.end = false
            }

            this.commentService.get(this.id, this.page, 10).then((result: any) => {
                this.result = result
                this.page++
                if (result && result.E && result.E.danhsach) {
                    _.each(result.E.danhsach, (item) => {
                        let itemDateArr = [
                            [item.tdpost.substring(0, 4), item.tdpost.substring(4, 6), item.tdpost.substring(6, 8)],
                            [item.tdpost.substring(6, 8), item.tdpost.substring(8, 10)]
                        ]
                        item.tdpost = itemDateArr[0].join('/') + ' ' + itemDateArr[1].join(':')
                    })
                    this.list = _.concat(this.list, result.E.danhsach)
                    if (result.E.danhsach.length < 10) {
                        this.end = true
                    }
                } else {
                    this.end = true
                }
                this.loading = false
                this.firstLoading = false
            }, (err: any) => {
                this.loading = false
                this.firstLoading = false
            })
        }
    }

    comment(myComment, item?: any) {
        this.commentService.put(this.id, this.type, this.name, myComment, item).then((result: any) => {
            this.end = false
        })
    }
}
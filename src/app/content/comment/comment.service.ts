import { Injectable } from '@angular/core'
import { HttpClient } from '../../app.http'
import * as _ from 'lodash'

@Injectable()

export class CommentService {
    user: any
    commentId: any

    constructor(
        private http: HttpClient
    ) { }

    /**
     * Comment to an object
     */
    put(id, type, name, comment, ref?: any) {
        const auth = this.http.getAuth()
        let uri = '06?&dv=2&id=' + id + '&khid=' + auth.A + '&kieudt=' + type + '&tendt=' + window.btoa(encodeURIComponent(name)) + '&nd=' + window.btoa(encodeURIComponent(comment))
        if (ref) {
            uri += '&ref_commentid=' + ref.commentid + '&ref_email=' + ref.email + '&ref_ten=' + ref.ten + '&ref_nd=' + ref.nd
        }
        return this.http.get(uri)
    }

    /**
     * Get all comments of an object
     */
    get(id, page, itemPerPage) {
        const auth = this.http.getAuth()
        return this.http.get('06?&dv=1&id=' + id + '&sotrang=' + page + '&spttrang=' + itemPerPage)
    }
}
import { Component } from '@angular/core'
import { AppService } from '../../../app.service'
import { CardsService } from '../cards.service'
import { HttpClient } from '../../../app.http'

@Component({
    selector: 'app-cards-product',
    templateUrl: './cards.product.component.html',
    styleUrls: ['./cards.product.component.css'],
    providers: [AppService, CardsService, HttpClient]
})

export class AppCardsProductComponent {
    constructor(
        private appService: AppService,
        private cardsService: CardsService,
    ) { }
}
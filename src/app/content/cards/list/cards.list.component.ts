import { Component } from '@angular/core'
import { AppService } from '../../../app.service'
import { CardsService } from '../cards.service'
import { HttpClient } from '../../../app.http'
import * as _ from 'lodash'

@Component({
    selector: 'app-cards-list',
    templateUrl: './cards.list.component.html',
    styleUrls: ['./cards.list.component.css'],
    providers: [AppService, CardsService, HttpClient]
})

export class AppCardsListComponent {
    result: any
    list = []
    loading = false
    firstLoading = true
    scrollItv: any
    page = 1
    end = false

    constructor(
        private appService: AppService,
        private cardsService: CardsService
    ) {
        this.page = 1
        this.getTop()
    }

    ngOnInit() {
        this.scrollItv = setInterval(() => {
            if (window.scrollY > document.body.scrollHeight - window.innerHeight - 300 && this.page > 1) {
                this.getTop()
            }
        }, 100)
    }

    ngOnDestroy() {
        clearInterval(this.scrollItv)
    }

    getTop() {
        if (!this.loading && !this.end) {
            this.loading = true
            if (this.page == 1) {
                this.list = []
                this.end = false
            }
            this.cardsService.getCards(this.page, 10).then((result: any) => {
                this.result = result
                this.page++
                if (result && result.E && result.E.danhsach) {
                    this.list = _.concat(this.list, result.E.danhsach)
                    if (result.E.danhsach.length < 10) {
                        this.end = true
                    }
                    console.log(this.list)
                } else {
                    this.end = true
                }
                this.loading = false
                this.firstLoading = false
            }, (err: any) => {
                this.loading = false
                this.firstLoading = false
            })
        }
    }
}
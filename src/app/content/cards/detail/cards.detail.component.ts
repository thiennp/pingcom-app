import { Component } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { AppService } from '../../../app.service'
import { CardsService } from '../cards.service'
import { HttpClient } from '../../../app.http'

@Component({
    selector: 'app-cards-detail',
    templateUrl: './cards.detail.component.html',
    styleUrls: ['./cards.detail.component.css'],
    providers: [AppService, CardsService, HttpClient]
})

export class AppCardsDetailComponent {
    id: any

    constructor(
        private appService: AppService,
        private cardsService: CardsService,
        private activatedRoute: ActivatedRoute,
    ) {
        this.activatedRoute.params.subscribe(params => {
            this.id = params.id
            this.getData(this.id)
        })
    }

    getData(id) {
        // this.cardsService.getDetail(id).then((result: any) => {
        //     if (result && result.E) {
        //         this.card = result.E
        //     }
        // })
    }
}
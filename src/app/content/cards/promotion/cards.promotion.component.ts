import { Component } from '@angular/core'
import { AppService } from '../../../app.service'
import { CardsService } from '../cards.service'
import { HttpClient } from '../../../app.http'

@Component({
    selector: 'app-cards-promotion',
    templateUrl: './cards.promotion.component.html',
    styleUrls: ['./cards.promotion.component.css'],
    providers: [AppService, CardsService, HttpClient]
})

export class AppCardsPromotionComponent {
    constructor(
        private appService: AppService,
        private cardsService: CardsService,
    ) { }
}
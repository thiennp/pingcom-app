import { Injectable } from '@angular/core'
import { HttpClient } from '../../app.http'
import * as _ from 'lodash'

@Injectable()

export class CardsService {
    constructor(
        private http: HttpClient
    ) { }

    /**
     * Get card list
     */
    getCards(page, itemsPerPage, nccid?: any) {
        const auth = this.http.getAuth()
        if (auth && auth.A) {
            if (!nccid) {
                return this.http.get('04?&dv=1&trang=' + page + '&soptmt=' + itemsPerPage + '&khid=' + auth.A)
            } else {
                return this.http.get('04?&dv=2&trang=' + page + '&soptmt=' + itemsPerPage + '&khid=' + auth.A + '&nccid=' + nccid)
            }
        }
    }
}
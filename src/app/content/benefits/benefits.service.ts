import { Injectable } from '@angular/core'
import { HttpClient } from '../../app.http'
import * as _ from 'lodash'

@Injectable()

export class BenefitsService {
    constructor(
        private http: HttpClient
    ) { }

    /**
     * Get baner
     */
    getBanner() {
        return this.http.get('01?dv=5&mtt=HANOI')
    }

    /**
     * Get new benefit
     */
    getNew(page, itemsPerPage, madm) {
        if (madm) {
            return this.http.get('01?&dv=16&sotrang=' + page + '&spttrang=' + itemsPerPage + '&madm=' + madm + '&kdl=3')
        } else {
            return this.http.get('01?&dv=16&sotrang=' + page + '&spttrang=' + itemsPerPage + '&kdl=3')
        }
    }

    /**
     * Get nearby benefit
     */
    getNearby(page, itemsPerPage, coords) {
        let lat = coords.latitude
        let lon = coords.longitude
        return this.http.get('01?&dv=21&sotrang=' + page + '&spttrang=' + itemsPerPage + '&vd=' + lat + '&kd=' + lon)
    }

    /**
     * Get top buy benefit
     */
    getTop(page, itemsPerPage, madm) {
        if (madm) {
            return this.http.get('01?&dv=16&sotrang=' + page + '&spttrang=' + itemsPerPage + '&madm=' + madm + '&kdl=4')
        } else {
            return this.http.get('01?&dv=16&sotrang=' + page + '&spttrang=' + itemsPerPage + '&kdl=4')
        }
    }

    /**
     * Get benefit's detail
     */
    getDetail(id) {
        return this.http.get('01?dv=17&kenh=1&id=' + id)
    }
}
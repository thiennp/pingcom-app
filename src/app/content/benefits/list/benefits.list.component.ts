import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { HttpClient } from '../../../app.http'
import { AppService } from '../../../app.service'
import { BenefitsService } from '../benefits.service'

@Component({
    selector: 'app-list-benefits',
    templateUrl: './benefits.list.component.html',
    styleUrls: ['./benefits.list.component.css'],
    providers: [AppService, BenefitsService, HttpClient]
})

export class AppBenefitsListComponent {
    bannerList: any
    activeItemIndex = 0
    primary = true

    constructor(
        private appService: AppService,
        private benefitsService: BenefitsService,
        private route: Router
    ) {
        this.benefitsService.getBanner().then((result: any) => {
            this.appService.log(result, 3)
            try {
                this.bannerList = result.E.danhsach
                setInterval(() => {
                    this.activeItemIndex++
                    if (this.activeItemIndex >= this.bannerList.length) {
                        this.activeItemIndex = 0
                    }
                }, 5000)
            } catch (err) {
                // Do nothing
            }
        })
        this.route = route
    }
}
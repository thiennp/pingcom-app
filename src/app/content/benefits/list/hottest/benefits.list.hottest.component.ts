import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { BenefitsService } from '../../benefits.service'
import * as _ from 'lodash'

@Component({
    selector: 'app-benefits-list-hottest',
    templateUrl: './benefits.list.hottest.component.html',
    styleUrls: ['./benefits.list.hottest.component.css'],
    providers: [BenefitsService]
})

export class AppBenefitsListHottestComponent {
    result: any
    list = []
    loading = false
    firstLoading = true
    scrollItv: any
    page = 1
    end = false
    constructor(
        private benefitsService: BenefitsService
    ) {
        this.page = 1
        this.getTop()
    }

    ngOnInit() {
        this.scrollItv = setInterval(() => {
            if (window.scrollY > document.body.scrollHeight - window.innerHeight - 300 && this.page > 1) {
                this.getTop()
            }
        }, 100)
    }

    ngOnDestroy() {
        clearInterval(this.scrollItv)
    }

    getTop() {
        if (!this.loading && !this.end) {
            this.loading = true
            if (this.page == 1) {
                this.list = []
                this.end = false
            }
            this.benefitsService.getTop(this.page, 10, null).then((result: any) => {
                this.result = result
                this.page++
                if (result && result.E && result.E.danhsach) {
                    this.list = _.concat(this.list, result.E.danhsach)
                    if (result.E.danhsach.length < 10) {
                        this.end = true
                    }
                } else {
                    this.end = true
                }
                this.loading = false
                this.firstLoading = false
            }, (err: any) => {
                this.loading = false
                this.firstLoading = false
            })
        }
    }
}
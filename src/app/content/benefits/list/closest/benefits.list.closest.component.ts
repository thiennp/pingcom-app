import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { BenefitsService } from '../../benefits.service'
import * as _ from 'lodash'

@Component({
    selector: 'app-benefits-closest',
    templateUrl: './benefits.list.closest.component.html',
    styleUrls: ['./benefits.list.closest.component.css'],
    providers: [BenefitsService]
})

export class AppBenefitsListClosestComponent {
    coords: Array<any>
    result: any
    list = []
    loading = false
    firstLoading = true
    scrollItv: any
    page = 1
    end = false
    constructor(
        private benefitsService: BenefitsService
    ) {
        navigator.geolocation.getCurrentPosition((pos: any) => {
            this.coords = pos.coords
            this.page = 1
            this.getNearby()
        }, err => {
            this.firstLoading = false
        })
    }

    ngOnInit() {
        this.scrollItv = setInterval(() => {
            if (window.scrollY > document.body.scrollHeight - window.innerHeight - 300 && this.page > 1) {
                this.getNearby()
            }
        }, 100)
    }

    ngOnDestroy() {
        clearInterval(this.scrollItv)
    }

    getNearby() {
        if (!this.loading && !this.end) {
            this.loading = true
            if (this.page == 1) {
                this.list = []
                this.end = false
            }
            this.benefitsService.getNearby(this.page, 10, this.coords).then((result: any) => {
                this.result = result
                this.page++
                if (result && result.E && result.E.danhsach) {
                    this.list = _.concat(this.list, result.E.danhsach)
                    if (result.E.danhsach.length < 10) {
                        this.end = true
                    }
                } else {
                    this.end = true
                }
                this.loading = false
                this.firstLoading = false
            }, (err: any) => {
                this.loading = false
                this.firstLoading = false
            })
        }
    }
}
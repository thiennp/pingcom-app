import { Component } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { HttpClient } from '../../../app.http'
import { AppService } from '../../../app.service'
import { BenefitsService } from '../benefits.service'

@Component({
    selector: 'app-benefits-detail',
    templateUrl: './benefits.detail.component.html',
    styleUrls: ['./benefits.detail.component.css'],
    providers: [AppService, BenefitsService, HttpClient]
})

export class AppBenefitsDetailComponent {
    id: string
    benefit: any
    kmStart: any
    kmStartArr: any

    constructor(
        private benefitsService: BenefitsService,
        private activatedRoute: ActivatedRoute,
    ) {
        this.activatedRoute.params.subscribe(params => {
            this.id = params.id
            this.getData(this.id)
        })
    }

    getData(id) {
        this.benefitsService.getDetail(id).then((result: any) => {
            if (result && result.E) {
                this.benefit = result.E
                this.kmStartArr = [
                    [this.benefit.chitiet.tdbdkhuyenmai.substring(0, 4), this.benefit.chitiet.tdbdkhuyenmai.substring(4, 6), this.benefit.chitiet.tdbdkhuyenmai.substring(6, 8)],
                    [this.benefit.chitiet.tdbdkhuyenmai.substring(6, 8), this.benefit.chitiet.tdbdkhuyenmai.substring(8, 10)]
                ]
                this.kmStart = this.kmStartArr[0].join('/') + ' ' + this.kmStartArr[1].join(':')
            }
        })
    }
}
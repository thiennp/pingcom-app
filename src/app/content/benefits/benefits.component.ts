import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { AppService } from '../../app.service'
import { BenefitsService } from './benefits.service'

@Component({
    selector: 'app-benefits',
    templateUrl: './benefits.component.html',
    styleUrls: ['./benefits.component.css'],
    providers: [AppService, BenefitsService]
})

export class AppBenefitsComponent {
    constructor(
    ) { }
}
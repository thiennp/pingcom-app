import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { NotificationService } from '../notifications.service'
import * as _ from 'lodash'

@Component({
    selector: 'app-notifications-public',
    templateUrl: './notifications.public.component.html',
    styleUrls: ['./notifications.public.component.css'],
    providers: [NotificationService]
})

export class AppNotificationsPublicComponent {
    result: any
    list = []
    loading = false
    firstLoading = true
    scrollItv: any
    page = 1
    end = false
    constructor(
        private notificationService: NotificationService
    ) {
        this.page = 1
        
        this.getNews()
    }

    ngOnInit() {
        this.scrollItv = setInterval(() => {
            if (window.scrollY > document.body.scrollHeight - window.innerHeight - 300 && this.page > 1) {
                this.getNews()
            }
        }, 100)
    }

    ngOnDestroy() {
        clearInterval(this.scrollItv)
    }

    getNews() {
        if (!this.loading && !this.end) {
            this.loading = true
            if (this.page == 1) {
                this.list = []
                this.end = false
            }
            this.notificationService.getNews(this.page, 10)
            .then((result: any) => {
                    this.result = result
                    this.page++
                    if (result && result.E && result.E.danhsach) {
                        _.each(result.E.danhsach, item => {
                            const timeArr = [
                                [item.td.substring(0, 4), item.td.substring(4, 6), item.td.substring(6, 8)],
                                [item.td.substring(8, 10), item.td.substring(10, 12)]
                            ]
                            item.td = timeArr[0].join('/') + ' ' + timeArr[1].join(':')
                        })
                        this.list = _.concat(this.list, result.E.danhsach)
                        if (result.E.danhsach.length < 10) {
                            this.end = true
                        }
                    } else {
                        this.end = true
                    }
                    this.loading = false
                    this.firstLoading = false
                }, (err: any) => {
                    this.loading = false
                    this.firstLoading = false
                })
        }
    }
}
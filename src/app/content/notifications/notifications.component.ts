import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { HttpClient } from '../../app.http'
import { AppService } from '../../app.service'
import { UserService } from '../../common/user.service'

@Component({
    selector: 'app-notifications',
    templateUrl: './notifications.component.html',
    styleUrls: ['./notifications.component.css'],
    providers: [HttpClient, AppService, UserService]
})

export class AppNotificationsComponent {
    constructor(private route: Router) {
        this.route = route
    }
}
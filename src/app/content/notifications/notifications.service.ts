import { Injectable } from '@angular/core'
import { HttpClient } from '../../app.http'
import { UserService } from '../../common/user.service'
import * as _ from 'lodash'

@Injectable()

export class NotificationService {
    constructor(
        private http: HttpClient,
        private userService: UserService
    ) { }

    /**
     * Get new benefit
     */
    getNews(page, itemsPerPage) {
        const auth = this.http.getAuth()
        return new Promise((resolve, reject) => {
            if (auth && auth.A) {
                this.http.get('00?&dv=11b&trang=' + page + '&khid=' + auth.A + '&sptmt=' + itemsPerPage + '&loaitb=1&nn=vi')
                    .then(result => {
                        resolve(result)
                    })
                    .catch(err => {
                        reject(err)
                    })
            } else {
                this.userService.onShowLogin()
                reject(null)
            }
        })
    }

    /**
     * Get nearby benefit
     */
    getMine(page, itemsPerPage) {
        const auth = this.http.getAuth()
        return new Promise((resolve, reject) => {
            if (auth && auth.A) {
                this.http.get('00?&dv=11b&trang=' + page + '&khid=' + auth.A + '&sptmt=' + itemsPerPage + '&loaitb=2&nn=vi')
                    .then(result => {
                        resolve(result)
                    })
                    .catch(err => {
                        reject(err)
                    })
            } else {
                this.userService.onShowLogin()
                reject(null)
            }
        })
    }
}
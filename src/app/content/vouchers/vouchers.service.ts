import { Injectable } from '@angular/core'
import { HttpClient } from '../../app.http'
import * as _ from 'lodash'

@Injectable()

export class VouchersService {
    auth: any
    constructor(
        private http: HttpClient
    ) {
        this.auth = this.http.getAuth()
    }

    /**
     * Get new voucher
     */
    getMine(page, itemsPerPage) {
        return this.http.get('07?&dv=2&sotrang=' + page + '&spttrang=' + itemsPerPage + '&khid=' + this.auth.A)
    }

    /**
     * Get top buy voucher
     */
    getAll(page, itemsPerPage) {
        return this.http.get('07?&dv=1&sotrang=' + page + '&spttrang=' + itemsPerPage + '&khid=' + this.auth.A)
    }

    /**
     * Get voucher's detail
     */
    getDetail(id) {
        return new Promise((resolve, reject) => {
            this.http.get('07?dv=3&khid=' + this.auth.A + '&voucherid=' + id)
                .then((result: any) => {
                    if (result && result.E && result.E.chitiet && result.E.chitiet.nccid) {
                        this.http.get('01?dv=1&sotrang=1&spttrang=999&id=' + result.E.chitiet.nccid)
                            .then((res: any) => {
                                if (res && res.E && res.E.danhsach) {
                                    result.E.locations = res.E.danhsach
                                } else {
                                    result.E.locations = []
                                }
                                resolve(result)
                            })
                            .catch(err => {
                                reject(err)
                            })
                    } else {
                        result.E.locations = []
                        resolve(result)
                    }
                })
                .catch(err => {
                    reject(err)
                })
        })
    }
}
import { Component } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { HttpClient } from '../../../app.http'
import { AppService } from '../../../app.service'
import { VouchersService } from '../vouchers.service'

@Component({
    selector: 'app-vouchers-detail',
    templateUrl: './vouchers.detail.component.html',
    styleUrls: ['./vouchers.detail.component.css'],
    providers: [AppService, VouchersService, HttpClient]
})

export class AppVouchersDetailComponent {
    id: string
    voucher: any
    kmStart: any
    kmStartArr: any
    coords: any
    loading: Boolean

    constructor(
        private vouchersService: VouchersService,
        private activatedRoute: ActivatedRoute,
        private appService: AppService,
    ) {
        this.activatedRoute.params.subscribe(params => {
            this.id = params.id
        })
        this.loading = true

        navigator.geolocation.getCurrentPosition((pos: any) => {
            this.coords = pos.coords
            this.getData(this.id)
        }, err => {
            this.getData(this.id)
        })
    }

    getData(id) {
        this.vouchersService.getDetail(id).then((result: any) => {
            if (result && result.E) {
                this.voucher = result.E
                this.kmStartArr = [
                    [this.voucher.chitiet.tdbatdau.substring(0, 4), this.voucher.chitiet.tdbatdau.substring(4, 6), this.voucher.chitiet.tdbatdau.substring(6, 8)],
                    [this.voucher.chitiet.tdbatdau.substring(6, 8), this.voucher.chitiet.tdbatdau.substring(8, 10)]
                ]
                this.kmStart = this.kmStartArr[0].join('/') + ' ' + this.kmStartArr[1].join(':')
            }
            this.loading = false
        })
    }

    getNumber(num) {
        let arr = []
        for (let i = 0; i < num; i++) {
            arr.push(i)
        }
        return arr
    }

    getDistance(lat, lon) {
        if (this.coords) {
            return this.appService.getDistanceFromLatLonInKm(this.coords.latitude, this.coords.longitude, lat, lon)
        }
    }
}
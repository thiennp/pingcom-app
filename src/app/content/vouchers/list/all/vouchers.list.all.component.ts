import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { VouchersService } from '../../vouchers.service'
import * as _ from 'lodash'
import * as moment from 'moment'

@Component({
    selector: 'app-vouchers-all',
    templateUrl: './vouchers.list.all.component.html',
    styleUrls: ['./vouchers.list.all.component.css'],
    providers: [VouchersService]
})

export class AppVouchersListAllComponent {
    coords: Array<any>
    result: any
    list = []
    loading = false
    firstLoading = true
    scrollItv: any
    page = 1
    end = false
    constructor(
        private vouchersService: VouchersService
    ) {
        navigator.geolocation.getCurrentPosition((pos: any) => {
            this.coords = pos.coords
            this.page = 1
            this.getAll()
        }, err => {
            this.firstLoading = false
        })
    }

    ngOnInit() {
        this.scrollItv = setInterval(() => {
            if (window.scrollY > document.body.scrollHeight - window.innerHeight - 300 && this.page > 1) {
                this.getAll()
            }
        }, 100)
    }

    ngOnDestroy() {
        clearInterval(this.scrollItv)
    }

    getAll() {
        if (!this.loading && !this.end) {
            this.loading = true
            if (this.page == 1) {
                this.list = []
                this.end = false
            }
            this.vouchersService.getAll(this.page, 10).then((result: any) => {
                this.result = result
                this.page++
                if (result && result.E && result.E.danhsach) {
                    _.each(result.E.danhsach, (item) => {
                        item.tdbatdau = moment(item.tdbatdau, 'x')
                        item.tdketthuc = moment(item.tdbatdau, 'x')
                    })
                    this.list = _.concat(this.list, result.E.danhsach)
                    if (result.E.danhsach.length < 10) {
                        this.end = true
                    }
                } else {
                    this.end = true
                }
                this.loading = false
                this.firstLoading = false
                this.loading = false
                this.firstLoading = false
            })
        }
    }
}
import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { HttpClient } from '../../../app.http'
import { AppService } from '../../../app.service'

@Component({
    selector: 'app-list-vouchers',
    templateUrl: './vouchers.list.component.html',
    styleUrls: ['./vouchers.list.component.css'],
    providers: [AppService, HttpClient]
})

export class AppVouchersListComponent {
    bannerList: any
    activeItemIndex = 0
    primary = true

    constructor(
        private route: Router
    ) {
        this.route = route
    }
}
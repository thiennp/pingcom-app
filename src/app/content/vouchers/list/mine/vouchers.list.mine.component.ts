import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { VouchersService } from '../../vouchers.service'
import * as _ from 'lodash'
import * as moment from 'moment'

@Component({
    selector: 'app-vouchers-list-mine',
    templateUrl: './vouchers.list.mine.component.html',
    styleUrls: ['./vouchers.list.mine.component.css'],
    providers: [VouchersService]
})

export class AppVouchersListMineComponent {
    result: any
    list = []
    loading = false
    firstLoading = true
    scrollItv: any
    page = 1
    end = false
    constructor(
        private vouchersService: VouchersService
    ) {
        this.page = 1
        this.getMine()
    }

    ngOnInit() {
        this.scrollItv = setInterval(() => {
            if (window.scrollY > document.body.scrollHeight - window.innerHeight - 300 && this.page > 1) {
                this.getMine()
            }
        }, 100)
    }

    ngOnDestroy() {
        clearInterval(this.scrollItv)
    }

    getMine() {
        if (!this.loading && !this.end) {
            this.loading = true
            if (this.page == 1) {
                this.list = []
                this.end = false
            }
            this.vouchersService.getMine(this.page, 10).then((result: any) => {
                this.result = result
                this.page++
                if (result && result.E && result.E.danhsach) {
                    _.each(result.E.danhsach, (item) => {
                        item.tdbatdau = moment(item.tdbatdau, 'x')
                        item.tdketthuc = moment(item.tdbatdau, 'x')
                    })
                    this.list = _.concat(this.list, result.E.danhsach)
                    if (result.E.danhsach.length < 10) {
                        this.end = true
                    }
                } else {
                    this.end = true
                }
                this.loading = false
                this.firstLoading = false
            }, (err: any) => {
                this.loading = false
                this.firstLoading = false
            })
        }
    }
}
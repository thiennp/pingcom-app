import { Component } from '@angular/core'
import { AppService } from '../../../../app.service'
import { ProfileService } from '../../profile.service'
import { HttpClient } from '../../../../app.http'
import * as _ from 'lodash'

@Component({
    selector: 'app-profile-history-point',
    templateUrl: './profile.history.point.component.html',
    styleUrls: ['./profile.history.point.component.css'],
    providers: [HttpClient, AppService, ProfileService]
})

export class AppProfileHistoryPointComponent {
    result: any
    list = []
    loading = false

    constructor(
        private appService: AppService,
        private profileService: ProfileService
    ) {
        this.getData()
    }

    getData() {
        this.loading = true
        this.list = []
        this.profileService.getHistoryPoint().then((result: any) => {
            this.result = result
            if (result && result.E && result.E.danhsach) {
                _.each(result.E.danhsach, (item) => {
                    let itemDateArr = [
                        [item.tdtao.substring(0, 4), item.tdtao.substring(4, 6), item.tdtao.substring(6, 8)],
                        [item.tdtao.substring(6, 8), item.tdtao.substring(8, 10)]
                    ]
                    item.tdtao = itemDateArr[0].join('/') + ' ' + itemDateArr[1].join(':')
                })  
                this.list = _.concat(this.list, result.E.danhsach)
            }
            this.loading = false
        }, (err: any) => {
            this.loading = false
        })
    }
}
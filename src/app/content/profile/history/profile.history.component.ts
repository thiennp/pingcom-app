import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { HttpClient } from '../../../app.http'
import { AppService } from '../../../app.service'

@Component({
    selector: 'app-profile-history',
    templateUrl: './profile.history.component.html',
    styleUrls: ['./profile.history.component.css'],
    providers: [HttpClient, AppService]
})

export class AppProfileHistoryComponent {
    activeItemIndex = 0
    constructor(
        private appService: AppService,
        private route: Router
    ) {
        this.route = route
    }
}
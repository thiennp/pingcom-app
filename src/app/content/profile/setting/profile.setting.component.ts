import { Component } from '@angular/core'
import { AppService } from '../../../app.service'
import { HttpClient } from '../../../app.http'
import { UserService } from '../../../common/user.service'
import { HeaderService } from '../../../header/header.service'

@Component({
    selector: 'app-profile-setting',
    templateUrl: './profile.setting.component.html',
    styleUrls: ['./profile.setting.component.css'],
    providers: [HeaderService, HttpClient, UserService, AppService]
})

export class AppProfileSettingComponent {
    user: any
    constructor(
        private appService: AppService,
        private userService: UserService,
        private headerService: HeaderService,
        private http: HttpClient
    ) {
        const auth = this.http.getAuth()
        if (auth) {
            try {
                this.user = auth.E
            } catch (err) {
                this.appService.log(err, 1)
            }
        }
        if (this.user) {
            console.log(this.user)
        }
    }
}
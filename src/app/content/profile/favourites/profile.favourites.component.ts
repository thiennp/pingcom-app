import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { HttpClient } from '../../../app.http'
import { AppService } from '../../../app.service'

@Component({
    selector: 'app-profile-favourites',
    templateUrl: './profile.favourites.component.html',
    styleUrls: ['./profile.favourites.component.css'],
    providers: [HttpClient, AppService]
})

export class AppProfileFavouritesComponent {
    activeItemIndex = 0
    constructor(
        private appService: AppService,
        private route: Router
    ) {
        this.route = route
    }
}
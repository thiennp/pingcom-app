import { Component } from '@angular/core'
import { AppService } from '../../../../app.service'
import { ProfileService } from '../../profile.service'
import { HttpClient } from '../../../../app.http'
import * as _ from 'lodash'

@Component({
    selector: 'app-profile-favourites-promotion',
    templateUrl: './profile.favourites.promotion.component.html',
    styleUrls: ['./profile.favourites.promotion.component.css'],
    providers: [HttpClient, AppService, ProfileService]
})

export class AppProfileFavouritesPromotionComponent {
    result: any
    list = []
    loading = false
    firstLoading = true
    scrollItv: any
    page = 1
    end = false

    constructor(
        private appService: AppService,
        private profileService: ProfileService
    ) {
        this.page = 1
        this.getTop()
    }

    ngOnInit() {
        this.scrollItv = setInterval(() => {
            if (window.scrollY > document.body.scrollHeight - window.innerHeight - 300 && this.page > 1) {
                this.getTop()
            }
        }, 100)
    }

    ngOnDestroy() {
        clearInterval(this.scrollItv)
    }

    getTop() {
        if (!this.loading && !this.end) {
            this.loading = true
            if (this.page == 1) {
                this.list = []
                this.end = false
            }
            this.profileService.getFavourites(this.page, 10).then((result: any) => {
                this.result = result
                this.page++
                if (result && result.E && result.E.danhsach) {
                    this.list = _.concat(this.list, result.E.danhsach)
                    if (result.E.danhsach.length < 10) {
                        this.end = true
                    }
                } else {
                    this.end = true
                }
                this.loading = false
                this.firstLoading = false
            }, (err: any) => {
                this.loading = false
                this.firstLoading = false
            })
        }
    }
}
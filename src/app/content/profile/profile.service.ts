import { Injectable } from '@angular/core'
import { HttpClient } from '../../app.http'
import * as _ from 'lodash'

@Injectable()

export class ProfileService {
    constructor(
        private http: HttpClient
    ) { }

    /**
     * Get favourite list
     */
    getFavourites(page, itemsPerPage) {
        const auth = this.http.getAuth()
        if (auth && auth.A) {
            return this.http.get('06?dv=7&kieudt=3&sotrang=' + page + '&spttrang=' + itemsPerPage + '&khid=' + auth.A)
        }
    }

    /**
     * Get history point list
     */
    getHistoryPoint() {
        const auth = this.http.getAuth()
        if (auth && auth.A) {
            return this.http.get('00?dv=10&khid=' + auth.A)
        }
    }

    /**
     * Get history pruduct list
     */
    getHistoryProduct() {
        const auth = this.http.getAuth()
        if (auth && auth.A) {
            return this.http.get('00?dv=9&khid=' + auth.A + '&kenhht=1')
        }
    }

    /**
     * Get history promotion list
     */
    getHistoryPromotion() {
        const auth = this.http.getAuth()
        if (auth && auth.A) {
            return this.http.get('00?dv=9&khid=' + auth.A + '&kenhht=2')
        }
    }

    /**
     * Get history gift list
     */
    getHistoryGift() {
        const auth = this.http.getAuth()
        if (auth && auth.A) {
            return this.http.get('00?dv=9&khid=' + auth.A + '&kenhht=3')
        }
    }

    /**
     * Get history voucher list
     */
    getHistoryVoucher(page, itemsPerPage) {
        const auth = this.http.getAuth()
        if (auth && auth.A) {
            return this.http.get('07?dv=2&sotrang=' + page + '&spttrang=' + itemsPerPage + '&khid=' + auth.A)
        }
    }

    /**
     * Log out
     */
    logOut() {
        const auth = this.http.getAuth()
        if (auth && auth.A) {
            return this.http.get('00?dv=8&&khid=' + auth.A)
        }
    }
}
import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { AppService } from '../../app.service'
import { ProfileService } from './profile.service'

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css'],
    providers: [AppService, ProfileService]
})

export class AppProfileComponent {
    constructor(
    ) {
    }
}
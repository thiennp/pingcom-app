import { Component } from '@angular/core'
import { AppService } from '../../../app.service'
import { ProfileService } from '../profile.service'
import { HttpClient } from '../../../app.http'
import * as _ from 'lodash'

@Component({
    selector: 'app-profile-list',
    templateUrl: './profile.list.component.html',
    styleUrls: ['./profile.list.component.css'],
    providers: [HttpClient, AppService, ProfileService]
})

export class AppProfileListComponent {
    user: any

    constructor(
        private appService: AppService,
        private profileService: ProfileService,
        private http: HttpClient
    ) {
        const auth = this.http.getAuth()
        if (auth) {
            try {
                this.user = auth.E
            } catch (err) {
                this.appService.log(err, 1)
            }
        }
    }
}
import { Injectable } from '@angular/core'

@Injectable()

export class AppService {
    data: any

    constructor(
    ) {
        this.data = {
            state: []
        }
    }

    /**
     * Log console (on dev)
     * @param message
     */
    log(message, level) {
        switch (level) {
            case 1:
            case 2:
            case 3:
                console.log(message)
                break
        }
    }

    /**
     * Display error when getting list of items unsuccessfuly
     * @param message
     */
    errorHandleGettingAll(message) {
        if (message) {
            this.log('Error getting items. Please contact us.', 1)
        } else {
            this.log('Error getting items, reason:' + message + '. Please contact us!', 1)
        }
    }

    /**
     * Display error when saving unsuccessfuly
     * @param message
     */
    errorHandleSaving(message) {
        if (message) {
            this.log('Error saving item. Please contact us.', 1)
        } else {
            this.log('Error saving item, reason:' + message + '. Please contact us!', 1)
        }
    }

    /**
     * Display error when deleting unsuccessfuly
     * @param message
     */
    errorHandleDeleting(message) {
        if (message) {
            this.log('Error deleting item. Please contact us.', 1)
        } else {
            this.log('Error deleting item, reason:' + message + '. Please contact us!', 1)
        }
    }

    /**
     * Display error when creating unsuccessfuly
     * @param message
     */
    errorHandleCreating(message) {
        if (message) {
            this.log('Error creating item. Please contact us.', 1)
        } else {
            this.log('Error creating item, reason:' + message + '. Please contact us!', 1)
        }
    }

    /**
     * Reload function when user click after an error in getting data
     */
    onReload() {
        return window.location.reload()
    }

    /**
     * calculate distance betwwen 2 locaiton
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     */
    getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = this.deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
            ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }

    deg2rad(deg) {
        return deg * (Math.PI / 180)
    }
}
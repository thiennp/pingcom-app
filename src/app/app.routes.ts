import { AppProfileComponent } from './content/profile/profile.component'
import { AppProfileListComponent } from './content/profile/list/profile.list.component'
import { AppProfileSettingComponent } from './content/profile/setting/profile.setting.component'
import { AppProfileFavouritesComponent } from './content/profile/favourites/profile.favourites.component'
import { AppProfileFavouritesProductComponent } from './content/profile/favourites/product/profile.favourites.product.component'
import { AppProfileFavouritesPromotionComponent } from './content/profile/favourites/promotion/profile.favourites.promotion.component'
import { AppProfileHistoryComponent } from './content/profile/history/profile.history.component'
import { AppProfileHistoryPointComponent } from './content/profile/history/point/profile.history.point.component'
import { AppProfileHistoryProductComponent } from './content/profile/history/product/profile.history.product.component'
import { AppProfileHistoryPromotionComponent } from './content/profile/history/promotion/profile.history.promotion.component'
import { AppProfileHistoryGiftComponent } from './content/profile/history/gift/profile.history.gift.component'
import { AppProfileHistoryVoucherComponent } from './content/profile/history/voucher/profile.history.voucher.component'
import { AppBenefitsComponent } from './content/benefits/benefits.component'
import { AppBenefitsListComponent } from './content/benefits/list/benefits.list.component'
import { AppBenefitsListLatestComponent } from './content/benefits/list/latest/benefits.list.latest.component'
import { AppBenefitsListClosestComponent } from './content/benefits/list/closest/benefits.list.closest.component'
import { AppBenefitsListHottestComponent } from './content/benefits/list/hottest/benefits.list.hottest.component'
import { AppBenefitsDetailComponent } from './content/benefits/detail/benefits.detail.component'
import { AppCardsComponent } from './content/cards/cards.component'
import { AppCardsListComponent } from './content/cards/list/cards.list.component'
import { AppCardsNewComponent } from './content/cards/new/cards.new.component'
import { AppCardsDetailComponent } from './content/cards/detail/cards.detail.component'
import { AppCardsProductComponent } from './content/cards/product/cards.product.component'
import { AppCardsPromotionComponent } from './content/cards/promotion/cards.promotion.component'
import { AppVouchersComponent } from './content/vouchers/vouchers.component'
import { AppVouchersListComponent } from './content/vouchers/list/vouchers.list.component'
import { AppVouchersListMineComponent } from './content/vouchers/list/mine/vouchers.list.mine.component'
import { AppVouchersListAllComponent } from './content/vouchers/list/all/vouchers.list.all.component'
import { AppVouchersDetailComponent } from './content/vouchers/detail/vouchers.detail.component'
import { AppGiftsComponent } from './content/gifts/gifts.component'
import { AppNotificationsComponent } from './content/notifications/notifications.component'
import { AppNotificationsPublicComponent } from './content/notifications/public/notifications.public.component'
import { AppNotificationsPersonalComponent } from './content/notifications/personal/notifications.personal.component'

import { AppCodeComponent } from './content/code/code.component'

import { AppIconsComponent } from './supports/icons/icons.component'

export const AppRoutes = [
    {
        path: '',
        redirectTo: '/benefits/list/latest',
        pathMatch: 'full'
    },
    {
        path: 'profile',
        component: AppProfileComponent,
        children: [
            {
                path: 'list',
                component: AppProfileListComponent
            },
            {
                path: 'setting',
                component: AppProfileSettingComponent
            },
            {
                path: 'favourites',
                component: AppProfileFavouritesComponent,
                children: [
                    {
                        path: 'product',
                        component: AppProfileFavouritesProductComponent
                    },
                    {
                        path: 'promotion',
                        component: AppProfileFavouritesPromotionComponent
                    }
                ]
            },
            {
                path: 'history',
                component: AppProfileHistoryComponent,
                children: [
                    {
                        path: 'point',
                        component: AppProfileHistoryPointComponent
                    },
                    {
                        path: 'product',
                        component: AppProfileHistoryProductComponent
                    },
                    {
                        path: 'promotion',
                        component: AppProfileHistoryPromotionComponent
                    },
                    {
                        path: 'gift',
                        component: AppProfileHistoryGiftComponent
                    },
                    {
                        path: 'voucher',
                        component: AppProfileHistoryVoucherComponent
                    }
                ]
            }
        ]
    },
    {
        path: 'benefits',
        component: AppBenefitsComponent,
        children: [
            {
                path: 'list',
                component: AppBenefitsListComponent,
                children: [
                    {
                        path: 'closest',
                        component: AppBenefitsListClosestComponent
                    },
                    {
                        path: 'hottest',
                        component: AppBenefitsListHottestComponent
                    },
                    {
                        path: 'latest',
                        component: AppBenefitsListLatestComponent
                    }
                ]
            },
            {
                path: 'detail/:id',
                component: AppBenefitsDetailComponent
            }
        ]
    },
    {
        path: 'cards',
        component: AppCardsComponent,
        children: [
            {
                path: 'list',
                component: AppCardsListComponent
            },
            {
                path: 'new',
                component: AppCardsNewComponent
            },
            {
                path: ':id/detail',
                component: AppCardsDetailComponent
            },
            {
                path: ':id/product',
                component: AppCardsProductComponent
            },
            {
                path: ':id/promotion',
                component: AppCardsPromotionComponent
            }
        ]
    },
    {
        path: 'vouchers',
        component: AppVouchersComponent,
        children: [
            {
                path: 'list',
                component: AppVouchersListComponent,
                children: [
                    {
                        path: 'mine',
                        component: AppVouchersListMineComponent
                    },
                    {
                        path: 'all',
                        component: AppVouchersListAllComponent
                    }
                ]
            },
            {
                path: 'detail/:id',
                component: AppVouchersDetailComponent
            }
        ]
    },
    {
        path: 'gifts',
        component: AppGiftsComponent
    },
    {
        path: 'notifications',
        component: AppNotificationsComponent,
        children: [
            {
                path: 'public',
                component: AppNotificationsPublicComponent
            },
            {
                path: 'personal',
                component: AppNotificationsPersonalComponent
            }
        ]
    },
    {
        path: 'code',
        component: AppCodeComponent
    },
    {
        path: 'icons',
        component: AppIconsComponent
    },
    {
        path: '**',
        redirectTo: '/benefits/list/latest',
        pathMatch: 'full'
    }
]
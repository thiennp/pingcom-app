import { Component } from '@angular/core'

@Component({
    selector: 'app-icons',
    templateUrl: './icons.component.html',
    styleUrls: ['./icons.component.css']
})

export class AppIconsComponent {
    private icons = []
    constructor() {
        var i = 1
        do {
            var extra = ''
            if (i < 10) {
                extra = '0'
            }
            this.icons.push('assets/img/svg/icon_SVG-' + extra + i + '.svg')
            i++
        }
        while (i < 82)
    }
}
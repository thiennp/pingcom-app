import { EventEmitter, Output } from '@angular/core'
import { Injectable } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'

// Login dialog
import { AppLoginComponent } from '../login/login.component'
import { HttpClient } from '../app.http'

import * as moment from 'moment/moment'
import * as _ from 'lodash'

@Injectable()

export class UserService {
    @Output() evtLogIn: EventEmitter<any> = new EventEmitter()
    @Output() evtLogOut: EventEmitter<any> = new EventEmitter()
    @Output() evtSwitchRole: EventEmitter<any> = new EventEmitter()

    constructor(
        private http: HttpClient,
        private modal: NgbModal
    ) { }

    /*
     * Check if user is loggedIn
     */
    getCurrentUser() {
        return this.http.getAuth()
    }

    /**
     * Log out
     */
    onLogOut() {
        localStorage.removeItem('A')
        localStorage.removeItem('C')
        localStorage.removeItem('E')
        this.evtLogOut.emit(null)
    }

    /**
     * Show login - register dialog
     */
    onShowLogin() {
        const modalRef = this.modal.open(AppLoginComponent)
        let auth = {}

        modalRef.result
            .then(res => {
                _.each(res, (index) => {
                    const strArr = index.split('=')
                    auth[strArr[0]] = strArr[1]
                })
            })
    }
}
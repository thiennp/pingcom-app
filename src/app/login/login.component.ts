import { Component } from '@angular/core'
import { EventEmitter, Output } from '@angular/core'
import { Http } from '@angular/http'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'

import * as _ from 'lodash'

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})


export class AppLoginComponent {
    @Output() evtLogIn: EventEmitter<any> = new EventEmitter()

    user: any = {
        username: '+841683477698',
        password: 'OTY3MzYy'
    }
    logining: boolean

    constructor(
        private http: Http,
        private modalRef: NgbActiveModal
    ) { }

    login() {
        this.logining = true

        this.http.get('https://apiwapxlt.pingcom.vn/00?&dv=4&sdt=' + this.user.username + '&mk=' + this.user.password + '&madtqg=+84&nn=vi')
            .subscribe(data => {
                const dataResult = data['_body']
                const dataArrayStr = dataResult.split('::')
                _.each(dataArrayStr, (item) => {
                    const dataItemValue = item.split('=')
                    localStorage.setItem('pingcom-app-' + dataItemValue[0], dataItemValue[1])
                })
                this.logining = false
                this.evtLogIn.emit(null)
                this.modalRef.close(dataArrayStr)
            })
    }

    close() {
        this.modalRef.close()
    }
}
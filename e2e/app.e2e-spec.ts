import { PingcomAppPage } from './app.po';

describe('pingcom-app App', () => {
  let page: PingcomAppPage;

  beforeEach(() => {
    page = new PingcomAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
